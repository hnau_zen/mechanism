clean:
	rm -rf *.egg-info \
	rm -rf dist \
	rm -rf build
	
build: clean
	python3 setup.py sdist bdist_wheel
	
test_upload: build
	python3 -m twine upload --repository testpypi dist/*
	
upload: build
	python3 -m twine upload dist/*

